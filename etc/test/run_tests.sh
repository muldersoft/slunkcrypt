#!/bin/bash
set -e

if [ "$(find . -maxdepth 1 -type f -perm /111 -name 'slunkcrypt*' | sed -e 's/^[[:blank:]]*//;s/[[:blank:]]*$//' -e '/^$/d' | wc -l)" -lt 1 ]; then
	echo "No Slunkcrypt executable(s) found!"
	exit 1
fi

function check_file() {
	local checksum="$(sha512sum "${1}" | grep -Eo '^[[:alnum:]]+' | tr 'A-Z' 'a-z' | head -n 1)"
	if [ "${checksum}" != "${2}" ]; then
		echo "Error: Checksum of file \"${1}\" does not match !!!"
		echo "-> Expected: ${2}"
		echo "-> Computed: ${checksum}"
		exit 1
	fi
	echo "Checksum of file \"${1}\" is correct."
}

echo -e "\n------------------------------------------------------------------------------"
echo "Clean up"
echo -e "------------------------------------------------------------------------------\n"

rm -vf test-vectors.tar passphrase.txt sha512sums.txt
rm -vf test-*.enc rm -vf restored-*.png

echo -e "\n------------------------------------------------------------------------------"
echo "Download"
echo -e "------------------------------------------------------------------------------\n"

curl -kLf -o "test-vectors.tar" "https://sourceforge.net/projects/muldersoft/files/SlunkCrypt/test-vectors.tar/download"
printf "\n"

tar -xvf "test-vectors.tar"
printf "\n"

check_file "test-v100-1.enc" "372358b8bd8f31834da449b456ef0d62ceeba95b3ac9e87d36f81ac0292be3da29cc7d1df55a72473dd08211bb5bab748f8c8b57f25b336d22862f2af2b4d3b2"
check_file "test-v100-2.enc" "a0e97b8e0b78a2fde215eb7234cff253e4af5b9ae426aaebfa85ddc84a79b3fa6ad1d7fad3b792610a4dd824ec56535dfb00a7352228fcefd011bf024af37dad"
check_file "test-v130-1.enc" "c13e6e001b08efba58909beed952957ea245a6f7fe52ad7022265f25f6f3a1c74423922f46cf171f2b23f948e4fde7004d83c89f95b62f825ff262cc7468bb48"
check_file "test-v130-2.enc" "48fe6ce4b82fb1c132d987b95576b6856deb43031047f669434347d9ffb8bc79a636e59548ffa18bd8031fbaeb034cb972a147087f49a61bf53a6b740434f3c6"

while IFS= read -r name; do
	echo -e "\n------------------------------------------------------------------------------"
	echo "${name}"
	echo -e "------------------------------------------------------------------------------\n"

	SLUNK_LEGACY_COMPAT=1 ./${name} -d 'pass:Q8YRL4_SFGt9m(>I,(h=fDaw' "test-v100-1.enc" "restored-v100-1.${name}.png"
	SLUNK_LEGACY_COMPAT=1 ./${name} -d 'pass:1$SfiHiQW*G|RbpR8trFR$sp' "test-v100-2.enc" "restored-v100-2.${name}.png"
	SLUNK_LEGACY_COMPAT=0 ./${name} -d 'pass:Q8YRL4_SFGt9m(>I,(h=fDaw' "test-v130-1.enc" "restored-v130-1.${name}.png"
	SLUNK_LEGACY_COMPAT=0 ./${name} -d 'pass:1$SfiHiQW*G|RbpR8trFR$sp' "test-v130-2.enc" "restored-v130-2.${name}.png"

	for i in 100 130; do
		for j in {1..2}; do
			check_file "restored-v${i}-${j}.${name}.png" "658a566d279070546dcb385f0ae1f236058edfe246caaaa149b8de448b788616cf78f229af7e70c3a06a23b649540aa5f53927f9a959a957b9b8b0ddf25063b1"
		done
	done
done < <(find . -maxdepth 1 -type f -perm /111 -name 'slunkcrypt*' -exec basename "{}" \; | sed -e 's/^[[:blank:]]*//;s/[[:blank:]]*$//' -e '/^$/d')

echo -e "\nEverything completed successfully.\n"
