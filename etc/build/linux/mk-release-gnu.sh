#!/bin/bash
set -e
cd -- "$(dirname -- "${BASH_SOURCE[0]}")/../../.."

function mk_slunk() {
	if [[ -n "${2}" && -z "$(which "${2}-gcc")" ]]; then
		echo "Warning: Cross-compiler for target \"${2}\" not found -> skipping!"
		return
	fi
	local command="make -B CC=${2}-gcc STRIP=1 FLTO=1"
	$BASH -x -c "${command}${3:+ ${3}}"
	cp -vf "frontend/bin/slunkcrypt" "out/slunkcrypt-${1}"
}

if [[ "$OSTYPE" != "linux"* ]]; then
	echo "This script is supposed to run on the linux platform !!!"
	exit 1
fi

rm -rf "out" && mkdir -p "out"

mk_slunk "x86_64" "x86_64-linux-gnu" "MARCH=x86-64 MTUNE=nocona"
mk_slunk "i686"   "i686-linux-gnu"   "MARCH=pentiumpro MTUNE=pentium3"

mk_slunk "arm64"      "aarch64-linux-gnu"
mk_slunk "armel"      "arm-linux-gnueabi"
mk_slunk "armhf"      "arm-linux-gnueabihf"
mk_slunk "mips"       "mips-linux-gnu"
mk_slunk "mips64"     "mips64-linux-gnuabi64"
mk_slunk "mips64el"   "mips64el-linux-gnuabi64"
mk_slunk "mips64r6"   "mipsisa64r6-linux-gnuabi64"
mk_slunk "mips64r6el" "mipsisa64r6el-linux-gnuabi64"
mk_slunk "mipsel"     "mipsel-linux-gnu"
mk_slunk "mipsr6"     "mipsisa32r6-linux-gnu"
mk_slunk "mipsr6el"   "mipsisa32r6el-linux-gnu"
mk_slunk "powerpc"    "powerpc-linux-gnu"
mk_slunk "ppc64le"    "powerpc64le-linux-gnu"
mk_slunk "riscv64"    "riscv64-linux-gnu"
mk_slunk "s390x"      "s390x-linux-gnu"

./etc/build/build_info.sh "x86_64-linux-gnu-gcc" > "out/.build_info"

printf "\033[1;32m\nBuild completed successfully.\033[0m\n\n"
