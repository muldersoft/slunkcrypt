#!/bin/bash
# See "etc/build/linux/mk-musl.sh" in order to build musl libc!
set -e
cd -- "$(dirname -- "${BASH_SOURCE[0]}")/../../.."

function mk_slunk() {
	if [ ! -e "/usr/local/musl/${1}/bin/musl-gcc" ]; then
		echo "Warning: Musl libc compiler for target \"${1}\" not found -> skipping!"
		return
	fi
	local command="make -B CC=/usr/local/musl/${1}/bin/musl-gcc STATIC=1 STRIP=1 FLTO=1"
	$BASH -x -c "${command}${2:+ ${2}}"
	cp -vf "frontend/bin/slunkcrypt" "out/slunkcrypt-${1}"
}

if [[ "$OSTYPE" != "linux"* ]]; then
	echo "This script is supposed to run on the linux platform !!!"
	exit 1
fi

rm -rf "out" && mkdir -p "out"

mk_slunk "x86_64" "MARCH=x86-64 MTUNE=nocona"
mk_slunk "i686"   "MARCH=pentiumpro MTUNE=pentium3"

mk_slunk "arm64"
mk_slunk "armel"
mk_slunk "armhf"
mk_slunk "mips"
mk_slunk "mips64"
mk_slunk "mips64el"
mk_slunk "mips64r6"
mk_slunk "mips64r6el"
mk_slunk "mipsel"
mk_slunk "mipsr6"
mk_slunk "mipsr6el"
mk_slunk "riscv64"
mk_slunk "s390x"

./etc/build/build_info.sh "/usr/local/musl/x86_64/bin/musl-gcc" > "out/.build_info"

printf "\033[1;32m\nBuild completed successfully.\033[0m\n\n"
