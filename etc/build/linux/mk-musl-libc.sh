#!/bin/bash
# Prerequisites: sudo apt install crossbuild-essential-*
set -e
cd -- "$(dirname -- "${BASH_SOURCE[0]}")"

function mk_musl() {
	if [[ -n "${2}" && -z "$(which "${2}-gcc")" ]]; then
		echo "Warning: Cross-compiler for target \"${2}\" not found -> skipping!"
		return
	fi
	local outdir="/usr/local/musl/${1}"
	local build="musl-build-${1}"
	rm -rf "${build}" && mkdir -p "${build}"
	tar -xvf "musl-latest.tar.gz" --strip-components=1 -C "${build}"
	pushd "${build}"
	local optdirs="$(find './src' -mindepth 1 -maxdepth 1 -type d -printf '%f,' | sed 's/,$//g')"
	if [ -z "${2}" ]; then
		./configure --enable-optimize="${optdirs}" --disable-shared --prefix="${outdir}"
	else
		./configure --enable-optimize="${optdirs}" --disable-shared --prefix="${outdir}" --host="${2}"
	fi
	make
	sudo rm -rf "${outdir}"
	sudo make install
	popd
	rm -rf "${build}"
}

if [ "$(gcc -dumpmachine)" != "x86_64-linux-gnu" ]; then
	echo "This script is supposed to run on the native \"x86_64-linux-gnu\" platform !!!"
	exit 1
fi

curl -vkf -o "musl-latest.tar.gz" "https://musl.libc.org/releases/musl-latest.tar.gz"

mk_musl arm64      aarch64-linux-gnu
mk_musl armel      arm-linux-gnueabi
mk_musl armhf      arm-linux-gnueabihf
mk_musl i686       i686-linux-gnu
mk_musl mips64el   mips64el-linux-gnuabi64
mk_musl mips64     mips64-linux-gnuabi64
mk_musl mips64r6el mipsisa64r6el-linux-gnuabi64
mk_musl mips64r6   mipsisa64r6-linux-gnuabi64
mk_musl mipsel     mipsel-linux-gnu
mk_musl mips       mips-linux-gnu
mk_musl mipsr6el   mipsisa32r6el-linux-gnu
mk_musl mipsr6     mipsisa32r6-linux-gnu
mk_musl riscv64    riscv64-linux-gnu
mk_musl s390x      s390x-linux-gnu
mk_musl x86_64     x86_64-linux-gnu

#mk_musl ppc64le powerpc64le-linux-gnu
#mk_musl powerpc powerpc-linux-gnu
