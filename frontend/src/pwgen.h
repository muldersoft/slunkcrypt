/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKAPP_PWGEN_H
#define INC_SLUNKAPP_PWGEN_H

#include "platform.h"

char *read_passphrase(const CHR *const file_name);
int weak_passphrase(const char *str);
int generate_passphrase(const size_t length);

#endif
