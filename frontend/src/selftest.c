/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

/* Internal */
#include "selftest.h"
#include "utils.h"
#include "crypt.h"
#include "test_data.h"
#include "blake2.h"

/* Library */
#include <slunkcrypt.h>

/* CRT */
#include <time.h>
#include <inttypes.h>
#include <ctype.h>
#include <assert.h>

/* Performance baseline (measured on Core i5 460M) */
#define PERF_FACTOR 104319.9

// ==========================================================================
// Self-test routines
// ==========================================================================

static int run_testcase(const char* const message, const uint64_t nonce, const uint64_t checksum_message, const uint64_t checksum_expected, const slunkparam_t *const param)
{
	static const char* const TEST_PASSPHRASE = "OrpheanBeh0lderScry!Doubt";

	int status, result = EXIT_FAILURE;
	const size_t length = strlen(message) + 1U;
	slunkcrypt_t ctx = SLUNKCRYPT_NULL;

	char* const text_temp = strdup(message);
	if (!text_temp)
	{
		FPUTS(T("\n\nWhoops: Failed to allocate text buffer!\n\n"), stderr);
		goto clean_up;
	}

	const uint64_t checksum_original = blake2s_compute((uint8_t*)text_temp, length);
	if (checksum_original != checksum_message)
	{
		FPRINTF(stderr, T("\n\nWhoops: Checksum mismatch detected! [expected: 0x%016") T(PRIX64) T(", actual: 0x%016") T(PRIX64) T("]\n\n"), checksum_message, checksum_original);
		goto clean_up;
	}

	ctx = slunkcrypt_alloc_ext(nonce, (const uint8_t*)TEST_PASSPHRASE, strlen(TEST_PASSPHRASE), SLUNKCRYPT_ENCRYPT, param);
	if (!ctx)
	{
		FPUTS(g_slunkcrypt_abort_flag ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to initialize encoder!\n\n"), stderr);
		goto clean_up;
	}

	status = slunkcrypt_inplace(ctx, (uint8_t*)text_temp, length);
	if (status != SLUNKCRYPT_SUCCESS)
	{
		FPUTS((status == SLUNKCRYPT_ABORTED) ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to encrypt the message!\n\n"), stderr);
		goto clean_up;
	}

	if (strncmp(message, text_temp, length) == 0)
	{
		FPUTS(T("\n\nWhoops: Encrypted message equals the original message!\n\n"), stderr);
		goto clean_up;
	}

	const uint64_t checksum_encrypted = blake2s_compute((uint8_t*)text_temp, length);
	if (checksum_encrypted != checksum_expected)
	{
		FPRINTF(stderr, T("\n\nWhoops: Checksum mismatch detected! [expected: 0x%016") T(PRIX64) T(", actual: 0x%016") T(PRIX64) T("]\n\n"), checksum_expected, checksum_encrypted);
		goto clean_up;
	}

	status = slunkcrypt_reset(ctx, nonce, (const uint8_t*)TEST_PASSPHRASE, strlen(TEST_PASSPHRASE), SLUNKCRYPT_DECRYPT);
	if (status != SLUNKCRYPT_SUCCESS)
	{
		FPUTS((status == SLUNKCRYPT_ABORTED) ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to initialize decoder!\n\n"), stderr);
		goto clean_up;
	}

	status = slunkcrypt_inplace(ctx, (uint8_t*)text_temp, length);
	if (status != SLUNKCRYPT_SUCCESS)
	{
		FPUTS((status == SLUNKCRYPT_ABORTED) ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to decrypt the message!\n\n"), stderr);
		goto clean_up;
	}

	if (memcmp(message, text_temp, length * sizeof(char)) != 0)
	{
		FPUTS(T("\n\nWhoops: Decrypted message does *not* match the original message!\n\n"), stderr);
		goto clean_up;
	}

	const uint64_t checksum_decrypted = blake2s_compute((uint8_t*)text_temp, length);
	if (checksum_decrypted != checksum_original)
	{
		FPRINTF(stderr, T("\n\nWhoops: Checksum mismatch detected! [expected: 0x%016") T(PRIX64) T(", actual: 0x%016") T(PRIX64) T("]\n\n"), checksum_original, checksum_decrypted);
		goto clean_up;
	}

	result = EXIT_SUCCESS;

clean_up:

	SLUNKCRYPT_SAFE_FREE(ctx);

	if (text_temp)
	{
		slunkcrypt_bzero(text_temp, strlen(text_temp));
		free(text_temp);
	}

	return result;
}

static int run_stresstest(const uint64_t nonce, const slunkparam_t *const param)
{
	static const char* const TEST_PASSPHRASE = "OrpheanBeh0lderScry!Doubt";
	static const size_t LENGTH = 134217689U, CHUNKZ_ENC = 8191U, CHUNKZ_DEC = 8179U;

	int status, result = EXIT_FAILURE;
	size_t offset, chunk_size;
	slunkcrypt_t ctx = SLUNKCRYPT_NULL;

	uint8_t* const buffer = (uint8_t*)malloc(LENGTH * sizeof(uint8_t));
	if (!buffer)
	{
		FPUTS(T("\n\nWhoops: Failed to allocate message buffer!\n\n"), stderr);
		goto clean_up;
	}

	if (slunkcrypt_random_bytes(buffer, LENGTH) != LENGTH)
	{
		FPUTS(T("\n\nWhoops: Failed to generate random message!\n\n"), stderr);
		goto clean_up;
	}

	const uint64_t checksum_original = blake2s_compute(buffer, LENGTH);

	ctx = slunkcrypt_alloc_ext(nonce, (const uint8_t*)TEST_PASSPHRASE, strlen(TEST_PASSPHRASE), SLUNKCRYPT_ENCRYPT, param);
	if (!ctx)
	{
		FPUTS(g_slunkcrypt_abort_flag ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to initialize encoder!\n\n"), stderr);
		goto clean_up;
	}

	for (offset = 0U; offset < LENGTH; offset += chunk_size)
	{
		chunk_size = ((LENGTH - offset) > CHUNKZ_ENC) ? CHUNKZ_ENC : (LENGTH - offset);
		status = slunkcrypt_inplace(ctx, buffer + offset, chunk_size);
		if (status != SLUNKCRYPT_SUCCESS)
		{
			FPUTS((status == SLUNKCRYPT_ABORTED) ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to encrypt the message!\n\n"), stderr);
			goto clean_up;
		}
	}

	status = slunkcrypt_reset(ctx, nonce, (const uint8_t*)TEST_PASSPHRASE, strlen(TEST_PASSPHRASE), SLUNKCRYPT_DECRYPT);
	if (status != SLUNKCRYPT_SUCCESS)
	{
		FPUTS((status == SLUNKCRYPT_ABORTED) ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to initialize decoder!\n\n"), stderr);
		goto clean_up;
	}

	for (offset = 0U; offset < LENGTH; offset += chunk_size)
	{
		chunk_size = ((LENGTH - offset) > CHUNKZ_DEC) ? CHUNKZ_DEC : (LENGTH - offset);
		status = slunkcrypt_inplace(ctx, buffer + offset, chunk_size);
		if (status != SLUNKCRYPT_SUCCESS)
		{
			FPUTS((status == SLUNKCRYPT_ABORTED) ? T("\n\nProcess interrupted!\n\n") : T("\n\nWhoops: Failed to decrypt the message!\n\n"), stderr);
			goto clean_up;
		}
	}

	const uint64_t checksum_decrypted = blake2s_compute(buffer, LENGTH);
	if (checksum_decrypted != checksum_original)
	{
		FPRINTF(stderr, T("\n\nWhoops: Checksum mismatch detected! [expected: 0x%016") T(PRIX64) T(", actual: 0x%016") T(PRIX64) T("]\n\n"), checksum_original, checksum_decrypted);
		goto clean_up;
	}

	result = EXIT_SUCCESS;

clean_up:

	SLUNKCRYPT_SAFE_FREE(ctx);

	if (buffer)
	{
		slunkcrypt_bzero(buffer, LENGTH);
		free(buffer);
	}

	return result;
}

int selftest_routine(const size_t thread_count, const int debug)
{
	static const size_t ITERATIONS = 2U;
	static const uint64_t TEST_NONCE[] = { 0x243F6A8885A308D3, 0x13198A2E03707344 };

	const struct
	{
		const char* text;
		uint64_t check_orig, check_encr[2U][2U];
	}
	TEST_STAGE[] =
	{
		{ TEST_DATA_0, TEST_CHCK_ORIG_0, { { TEST_CHCK_ENCR_0[0U], TEST_CHCK_ENCR_0[1U] }, { TEST_CHCK_ENCR_0[2U], TEST_CHCK_ENCR_0[3U] } } },
		{ TEST_DATA_1, TEST_CHCK_ORIG_1, { { TEST_CHCK_ENCR_1[0U], TEST_CHCK_ENCR_1[1U] }, { TEST_CHCK_ENCR_1[2U], TEST_CHCK_ENCR_1[3U] } } },
		{ TEST_DATA_2, TEST_CHCK_ORIG_2, { { TEST_CHCK_ENCR_2[0U], TEST_CHCK_ENCR_2[1U] }, { TEST_CHCK_ENCR_2[2U], TEST_CHCK_ENCR_2[3U] } } },
		{ TEST_DATA_3, TEST_CHCK_ORIG_3, { { TEST_CHCK_ENCR_3[0U], TEST_CHCK_ENCR_3[1U] }, { TEST_CHCK_ENCR_3[2U], TEST_CHCK_ENCR_3[3U] } } }
	};

	const size_t total = ARRAY_SIZE(TEST_NONCE) * (ITERATIONS + (ITERATIONS * ARRAY_SIZE(TEST_STAGE)));
	size_t count = 0U;
	uint64_t total_time = 0U;

	FPRINTF(stderr, T("Self-test is in progress, please be patient... stage %2u/%2u "), 0U, (unsigned)total);
	fflush(stderr);

	for (size_t i = 0U; i < ARRAY_SIZE(TEST_STAGE); ++i)
	{
		for (size_t j = 0U; j < ITERATIONS; ++j)
		{
			const slunkparam_t param = { SLUNKCRYPT_PARAM_VERSION, thread_count, (j > 0) ? SLUNKCRYPT_TRUE : SLUNKCRYPT_FALSE, debug };
			for (size_t k = 0U; k < ARRAY_SIZE(TEST_NONCE); ++k)
			{
				FPRINTF(stderr, T("\b\b\b\b\b\b%2u/%2u "), (unsigned)++count, (unsigned)total);
				fflush(stderr);
				const uint64_t clk_start = clock_read();
				if (run_testcase(TEST_STAGE[i].text, TEST_NONCE[k], TEST_STAGE[i].check_orig, TEST_STAGE[i].check_encr[j][k], &param) != EXIT_SUCCESS)
				{
					return EXIT_FAILURE;
				}
				total_time += clock_read() - clk_start;
			}
		}
	}

	const slunkparam_t param = { SLUNKCRYPT_PARAM_VERSION, thread_count, SLUNKCRYPT_FALSE, debug };
	for (size_t i = 0U; i < ITERATIONS; ++i)
	{
		for (size_t j = 0U; j < ARRAY_SIZE(TEST_NONCE); ++j)
		{
			FPRINTF(stderr, T("\b\b\b\b\b\b%2u/%2u "), (unsigned)++count, (unsigned)total);
			fflush(stderr);
			const uint64_t clk_start = clock_read();
			if (run_stresstest(TEST_NONCE[j], &param) != EXIT_SUCCESS)
			{
				return EXIT_FAILURE;
			}
			total_time += clock_read() - clk_start;
		}
	}

	FPRINTF(stderr, T("\b\b\b\b\b\b%2u/%2u\n\nCompleted successfully. (Score: %.0f)\n\n"), (unsigned)total, (unsigned)total, (double)clock_freq() / (double)total_time * PERF_FACTOR);
	fflush(stderr);

	return EXIT_SUCCESS;
}
