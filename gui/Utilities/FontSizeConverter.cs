﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System;
using System.Globalization;
using System.Windows.Data;

namespace com.muldersoft.slunkcrypt.gui.utils
{
    class FontSizeConverter : IValueConverter
    {
        public double Ratio { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double originalFontSize = (double)value;
            return originalFontSize * Ratio;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
