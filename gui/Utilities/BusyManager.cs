﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System;
using System.ComponentModel;

namespace com.muldersoft.slunkcrypt.gui.utils
{
    public interface INotifyBusyChanged : INotifyPropertyChanged
    {
        bool IsBusy { get; set; }
    }

    internal sealed class BusyManager : IDisposable
    {
        private volatile bool m_disposed = false;
        private readonly INotifyBusyChanged m_instance;

        public BusyManager(INotifyBusyChanged instance)
        {
            if (ReferenceEquals(m_instance = instance, null))
            {
                throw new ArgumentNullException();
            }
            if (m_instance.IsBusy)
            {
                throw new InvalidOperationException("Instance is busy!");
            }
            instance.IsBusy = true;
        }

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;
                m_instance.IsBusy = false;
            }
        }
    }
}
