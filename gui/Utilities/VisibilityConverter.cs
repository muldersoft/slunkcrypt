﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace com.muldersoft.slunkcrypt.gui.utils
{
    class VisibilityConverter : IValueConverter
    {
        public bool Inverse { get; set; } = false;
        public bool Collapse { get; set; } = true;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ?
                (Inverse ? (Collapse ? Visibility.Collapsed : Visibility.Hidden) : Visibility.Visible) :
                (Inverse ? Visibility.Visible : (Collapse ? Visibility.Collapsed : Visibility.Hidden)) ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
