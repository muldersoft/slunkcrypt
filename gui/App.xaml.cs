﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace com.muldersoft.slunkcrypt.gui
{
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ExceptionHandler);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                if (Array.Exists(e.Args, str => StrCaseCmp(str, "--render-mode=software")) || StrCaseCmp(Environment.GetEnvironmentVariable("SLUNK_RENDER_MODE"), "software"))
                {
                    RenderOptions.ProcessRenderMode = RenderMode.SoftwareOnly;
                }
            }
            catch { }
        }

        private static void ExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception exception;
            if (!ReferenceEquals(exception = args.ExceptionObject as Exception, null))
            {
                MessageBox.Show("Unhandeled exception error:\n\n" + exception.Message, exception.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            Environment.Exit(-1);
        }

        protected void FrameworkElement_PreviewUserInputEvent(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (!ReferenceEquals(element, null))
            {
                if (!element.IsHitTestVisible)
                {
                    e.Handled = true;
                }
            }
        }

        private static bool StrCaseCmp(string s1, string s2)
        {
            if ((!ReferenceEquals(s1, null)) && (!ReferenceEquals(s2, null)))
            {
                return string.Equals(s1.Trim(), s2.Trim(), StringComparison.OrdinalIgnoreCase);
            }
            return false;
        }
    }
}
