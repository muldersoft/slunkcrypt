﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace com.muldersoft.slunkcrypt.gui.ctrls
{
    /// <summary>
    /// Interaction logic for ImageToggleButton.xaml
    /// </summary>
    public partial class ImageToggleButton : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ImageToggleButton()
        {
            InitializeComponent();
            Toggle.Checked += OnCheckedChanged;
            Toggle.Unchecked += OnCheckedChanged;
        }

        public bool IsChecked
        {
            get
            {
                return Toggle.IsChecked.GetValueOrDefault();
            }
            set
            {
                Toggle.IsChecked = value;
            }
        }


        public ImageSource ImageSourceDefault
        {
            get
            {
                return Image_Default.Source;
            }
            set
            {
                Image_Default.Source = value;
            }
        }

        public ImageSource ImageSourceChecked
        {
            get
            {
                return Image_Checked.Source;
            }
            set
            {
                Image_Checked.Source = value;
            }
        }

        public string ToolTipDefault
        {
            get
            {
                return Image_Default.ToolTip as string;
            }
            set
            {
                Image_Default.ToolTip = value;
            }
        }

        public string ToolTipChecked
        {
            get
            {
                return Image_Checked.ToolTip as string;
            }
            set
            {
                Image_Checked.ToolTip = value;
            }
        }

        private void OnCheckedChanged(object sender, RoutedEventArgs e)
        {
            NotifyPropertyChanged("IsChecked");
        }

        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
