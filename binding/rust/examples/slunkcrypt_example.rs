/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

use slunkcrypt_rs::{SlunkCrypt, SlunkCryptPasswd};
use hex;
use rand::{thread_rng, RngCore};
use std::mem;

fn main() {
    // Create passphrase from string
    const PASSPHRASE: SlunkCryptPasswd = SlunkCryptPasswd::Str("OrpheanBeholderScryDoubt");

    // Fill buffer with random data (plaintext)
    let mut buffer = [ 0u8; 64 ];
    thread_rng().fill_bytes(&mut buffer);
    println!("Plaintext: {}", hex::encode(&buffer));

    // Encrypt the data in-place
    let (mut context_enc, nonce) = SlunkCrypt::init_encrypt(&PASSPHRASE, None).expect("Failed to create encryption context!");
    context_enc.inplace(&mut buffer).expect("Failed to encrypt data!");
    mem::drop(context_enc);
    println!("Encrypted: {}", hex::encode(&buffer));

    // Decrypt the data in-place
    let mut context_dec = SlunkCrypt::init_decrypt(&PASSPHRASE, nonce, None).expect("Failed to create decryption context!");
    context_dec.inplace(&mut buffer).expect("Failed to decrypt data!");
    mem::drop(context_dec);
    println!("Decrypted: {}", hex::encode(&buffer));
}
