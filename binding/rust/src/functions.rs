/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

use std::ffi::CStr;
use std::str;

use crate::error::SlunkCryptError;
use crate::ffi::libslunkcrypt::{self, SLUNKCRYPT_SUCCESS};

/// Generate a fresh random nonce, using a Cryptographically secure pseudorandom number generator.
pub fn generate_nonce() -> Result<u64, SlunkCryptError> {
    let mut nonce: u64 = u64::default();
    let retval = unsafe {
        libslunkcrypt::slunkcrypt_generate_nonce(&mut nonce)
    };
    match retval {
        SLUNKCRYPT_SUCCESS => Ok(nonce),
        _ => Err(SlunkCryptError::from_retval(retval, "failed to generate nonce!"))
    }
}

/// Returns the version of the native SlunkCrypt library.
pub fn get_version() -> (u16,u16,u16) {
    unsafe {(
        libslunkcrypt::SLUNKCRYPT_VERSION_MAJOR,
        libslunkcrypt::SLUNKCRYPT_VERSION_MINOR,
        libslunkcrypt::SLUNKCRYPT_VERSION_PATCH
    )}
}

// Returns the version of the native SlunkCrypt library.
pub fn get_build() -> &'static str {
    unsafe {
        CStr::from_ptr(libslunkcrypt::SLUNKCRYPT_BUILD).to_str().unwrap()
    }
}
