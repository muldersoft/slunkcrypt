/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import com.muldersoft.slunkcrypt.SlunkCryptException.ErrorKind;
import com.muldersoft.slunkcrypt.internal.SlunkCryptLibrary;
import com.muldersoft.slunkcrypt.internal.SlunkCryptParam;
import com.muldersoft.slunkcrypt.internal.types.Context;
import com.muldersoft.slunkcrypt.internal.types.SizeT;
import com.muldersoft.slunkcrypt.internal.types.UInt64;

/**
 * The abstract base class for all {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} implementations.
 */
abstract class AbstractSlunkCrypt implements SlunkCrypt {

	private static final int PASSWD_LENGTH_MIN =   8;
	private static final int PASSWD_LENGTH_MAX = 256;

	private static final String PROPERTY_THREAD_COUNT = "slunkcrypt.thread_count";

	private static final int SLUNKCRYPT_ENCRYPT = 0;
	private static final int SLUNKCRYPT_DECRYPT = 1;
	
	protected enum Mode {
		Encrypt(SLUNKCRYPT_ENCRYPT),
		Decrypt(SLUNKCRYPT_DECRYPT);

		private final int value;

		private Mode(final int value) {
			this.value = value;
		}

		public int intValue() {
			return value;
		}
	}

	private final Context context;
	private final AtomicBoolean closed = new AtomicBoolean(false);

	protected AbstractSlunkCrypt(final String passwd, final long nonce, final Mode mode) throws SlunkCryptException {
		this(Objects.requireNonNull(passwd, "passwd").getBytes(StandardCharsets.UTF_8), nonce, mode);
	}

	protected AbstractSlunkCrypt(final byte[] passwd, final long nonce, final Mode mode) throws SlunkCryptException {
		if ((mode != Mode.Encrypt) && (mode != Mode.Decrypt)) {
			throw new IllegalArgumentException("mode");
		}
		if ((passwd == null) || (passwd.length < PASSWD_LENGTH_MIN) || (passwd.length > PASSWD_LENGTH_MAX)) {
			throw new IllegalArgumentException("passwd");
		}

		final SlunkCryptParam parameters = new SlunkCryptParam();
		try {
			final String threadCount = System.getProperty(PROPERTY_THREAD_COUNT);
			if ((threadCount != null) && (!threadCount.isEmpty())) {
				parameters.thread_count = SizeT.of(Long.parseUnsignedLong(threadCount));
			}
		} catch(final Exception e) {
			throw new IllegalArgumentException(PROPERTY_THREAD_COUNT, e);
		}

		context = SlunkCryptLibrary.INSTANCE.slunkcrypt_alloc_ext(UInt64.of(nonce), passwd, SizeT.of(passwd.length), mode.intValue(), parameters);
		if (context.equals(Context.NULL)) {
			throw new SlunkCryptException(ErrorKind.Failure, "Failed to create SlunkCrypt context!");
		}
	}

	@Override
	public final byte[] process(final byte[] input) throws SlunkCryptException {
		if (input == null) {
			throw new NullPointerException("input");
		}

		final byte[] output = new byte[input.length];
		process(input, output);

		return output;
	}

	@Override
	public final void process(final byte[] input, final byte[] output) throws SlunkCryptException {
		if (input == null) {
			throw new NullPointerException("input");
		}
		if ((output == null) || (output.length < input.length)) {
			throw new IllegalArgumentException("output");
		}

		checkState();

		final int errorCode = SlunkCryptLibrary.INSTANCE.slunkcrypt_process(context, input, output, SizeT.of(input.length));
		if (!SlunkCryptException.isSuccess(errorCode)) {
			throw SlunkCryptException.mapErrorCode(errorCode, "Failed to process data!");
		}
	}

	@Override
	public final void inplace(final byte[] buffer) throws SlunkCryptException {
		if (buffer == null) {
			throw new NullPointerException("input");
		}

		checkState();

		final int errorCode = SlunkCryptLibrary.INSTANCE.slunkcrypt_inplace(context, buffer, SizeT.of(buffer.length));
		if (!SlunkCryptException.isSuccess(errorCode)) {
			throw SlunkCryptException.mapErrorCode(errorCode, "Failed to process data!");
		}
	}

	protected void reset(final String passwd, final long nonce, final Mode mode) throws SlunkCryptException {
		reset(Objects.requireNonNull(passwd, "passwd").getBytes(StandardCharsets.UTF_8), nonce, mode);
	}

	protected void reset(final byte[] passwd, final long nonce, final Mode mode) throws SlunkCryptException {
		if ((mode != Mode.Encrypt) && (mode != Mode.Decrypt)) {
			throw new IllegalArgumentException("mode");
		}
		if ((passwd == null) || (passwd.length < PASSWD_LENGTH_MIN) || (passwd.length > PASSWD_LENGTH_MAX)) {
			throw new IllegalArgumentException("passwd");
		}

		checkState();

		final int errorCode = SlunkCryptLibrary.INSTANCE.slunkcrypt_reset(context, UInt64.of(nonce), passwd, SizeT.of(passwd.length), mode.ordinal());
		if (!SlunkCryptException.isSuccess(errorCode)) {
			throw SlunkCryptException.mapErrorCode(errorCode, "Failed to process data!");
		}
	}

	@Override
	public void close() {
		if (closed.compareAndSet(false, true)) {
			SlunkCryptLibrary.INSTANCE.slunkcrypt_free(context);
		}
	}

	@Override
	public String toString() {
		return (!closed.get()) ? String.format("SlunkCrypt(0x%016X)", context.longValue()) : "SlunkCrypt(NULL)";
	}

	protected void checkState() {
		if (closed.get()) {
			throw new IllegalStateException("Instance is already closed!");
		}
	}
}
