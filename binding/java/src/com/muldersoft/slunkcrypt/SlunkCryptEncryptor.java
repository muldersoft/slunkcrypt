/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt;

/**
 * SlunkCrypt <i>encryption</i> context.
 * <p>Wraps a &ldquo;native&rdquo; SlunkCrypt context that was initialized in <b><i>encryption</i></b> mode and implements the {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} interface.</p>
 */
public class SlunkCryptEncryptor extends AbstractSlunkCrypt {

	private long nonce;

	/**
	 * Create a new {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} instance in <b><i>encryption</i></b> mode.
	 * <p><i>Note:</i> This method implicitly generates a new random nonce. Use {@link SlunkCryptEncryptor#getNonce() getNonce()} to retrieve the nonce value.</p>
	 * @param passwd the password to be used for encryption (UTF-8)
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 */
	public SlunkCryptEncryptor(final String passwd) throws SlunkCryptException {
		this(passwd, SlunkCrypt.generateNonce());
	}

	/**
	 * Create a new {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} instance in <b><i>encryption</i></b> mode.
	 * <p><i>Note:</i> This method implicitly generates a new random nonce. Use {@link SlunkCryptEncryptor#getNonce() getNonce()} to retrieve the nonce value.</p>
	 * @param passwd the password to be used for encryption (binary)
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 */
	public SlunkCryptEncryptor(final byte[] passwd) throws SlunkCryptException {
		this(passwd, SlunkCrypt.generateNonce());
	}

	private SlunkCryptEncryptor(final String passwd, final long nonce) throws SlunkCryptException {
		super(passwd, nonce, Mode.Encrypt);
		this.nonce = nonce;
	}

	private SlunkCryptEncryptor(final byte[] passwd, final long nonce) throws SlunkCryptException {
		super(passwd, nonce, Mode.Encrypt);
		this.nonce = nonce;
	}

	/**
	 * Re-initialize this {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor SlunkCryptEncryptor} instance.
	 * <p><i>Note:</i> This method implicitly generates a new random nonce. Use {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor#getNonce() getNonce()} to retrieve the nonce value.</p>
	 * @param passwd the password to be used for encryption (UTF-8)
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 * @throws IllegalStateException the instance has already been closed
	 */
	public void reset(final String passwd) throws SlunkCryptException {
		reset(passwd, nonce = SlunkCrypt.generateNonce(), Mode.Encrypt);
	}

	/**
	 * Re-initialize this {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor SlunkCryptEncryptor} instance.
	 * <p><i>Note:</i> This method implicitly generates a new random nonce. Use {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor#getNonce() getNonce()} to retrieve the nonce value.</p>
	 * @param passwd the password to be used for encryption (binary)
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 * @throws IllegalStateException the instance has already been closed
	 */
	public void reset(final byte[] passwd) throws SlunkCryptException {
		reset(passwd, nonce = SlunkCrypt.generateNonce(), Mode.Encrypt);
	}

	/**
	 * Return the nonce value that was generated for this {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor SlunkCryptEncryptor} instance.
	 * @return the nonce value
	 */
	public long getNonce() {
		return nonce;
	}
}
