/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.internal.types;

import com.sun.jna.IntegerType;

public class UInt16 extends IntegerType {

	private static final long serialVersionUID = 1L;
	
	public UInt16() {
		super(Short.BYTES);
	}
	
	private UInt16(final long value) {
		super(Short.BYTES, value, true);
	}
	
	public static UInt16 of(final long value) {
		return new UInt16(value);
	}
}
