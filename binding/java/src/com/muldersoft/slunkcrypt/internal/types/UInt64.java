/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.internal.types;

import com.sun.jna.IntegerType;

public class UInt64 extends IntegerType {

	private static final long serialVersionUID = 1L;
	
	public UInt64() {
		super(Long.BYTES);
	}
	
	private UInt64(final long value) {
		super(Long.BYTES, value, true);
	}

	public static UInt64 of(final long value) {
		return new UInt64(value);
	}
}
