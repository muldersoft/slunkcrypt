/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.internal.types;

import com.sun.jna.IntegerType;
import com.sun.jna.Native;

public class SizeT extends IntegerType {

	private static final long serialVersionUID = 1L;
	
	public SizeT() {
		super(Native.SIZE_T_SIZE);
	}
	
	private SizeT(final long value) {
		super(Native.SIZE_T_SIZE, value, true);
	}
	
	public static SizeT of(final long value) {
		return new SizeT(value);
	}
}
