package com.muldersoft.slunkcrypt.tests;

public class Utilities {

	private Utilities() {
		throw new IllegalAccessError();
	}

	public static String toHexString(final byte[] data) {
		final StringBuilder sb = new StringBuilder(Math.multiplyExact(2, data.length));
		for (final byte b : data) {
			sb.append(String.format("%02X", b));
		}
		return sb.toString();
	}
}
