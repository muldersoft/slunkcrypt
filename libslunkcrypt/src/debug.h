/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKCRYPT_DEBUG_H
#define INC_SLUNKCRYPT_DEBUG_H

void slunkcrypt_debug_write(const char *const message);
void slunkcrypt_debug_print(const char *const message, ...);

#define DBG_PRINTF(BUFF, FORMAT, ...) do \
{ \
	const int _strlen = sprintf((BUFF), (FORMAT), __VA_ARGS__); \
	if (_strlen > 0) { BUFF += _strlen; } \
} \
while (0)

#endif
