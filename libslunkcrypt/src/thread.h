/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKCRYPT_THREAD_H
#define INC_SLUNKCRYPT_THREAD_H

#include <stdlib.h>
#include <stdint.h>

typedef struct thrdpl_data_t thrdpl_t;
typedef void(*thrdpl_worker_t)(const size_t thread_count, void *const context, uint8_t *const buffer, const size_t length);

#ifndef SLUNKBUILD_NOTHREADS

#define MAX_THREADS 32U
thrdpl_t *slunkcrypt_thrdpl_create(const size_t count, const thrdpl_worker_t worker);
size_t slunkcrypt_thrdpl_count(const thrdpl_t *const thrdpl);
void slunkcrypt_thrdpl_init(thrdpl_t *const thrdpl, const size_t index, void *const context);
void slunkcrypt_thrdpl_exec(thrdpl_t *const thrdpl, uint8_t *const buffer, const size_t length);
void slunkcrypt_thrdpl_destroy(thrdpl_t *const thrdpl);

#else

#define MAX_THREADS 1U
#define slunkcrypt_thrdpl_create(X,Y) NULL
#define slunkcrypt_thrdpl_count(X)    0U
#define slunkcrypt_thrdpl_init(X,Y,Z) do { abort(); } while(0)
#define slunkcrypt_thrdpl_exec(X,Y,Z) do { abort(); } while(0)
#define slunkcrypt_thrdpl_destroy(X)  do { abort(); } while(0)

#endif
#endif
